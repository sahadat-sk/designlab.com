---
name: Sharing files and prototypes
---

Because components can be in multiple frames on a page, it’s best to link to the entire page when referencing as a design spec in the Pajamas documentation. Otherwise, you can link directly to the specific frame in issues, merge requests, and other places where a specific portion of the design is referenced.
